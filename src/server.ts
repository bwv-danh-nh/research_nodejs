import express, { Application, NextFunction, Request, Response } from 'express'
import sequelizeConnection from './config/database';
import BookRouter from './routes/book';
import CategoryRouter from './routes/category';


sequelizeConnection.sync().then(()=>{
    console.log('Connect to DB');
});
const app: Application = express()
const port = 3000

// Body parsing Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.get('/', async(req: Request, res: Response): Promise<Response> => {
    return res.status(200).send({ message: `Welcome to the typescript ! \n Endpoints available at http://localhost:${port}/api/v1` })
})

app.use(BookRouter);
app.use(CategoryRouter);

try {
    app.listen(port, () => {
        console.log(`Server running on http://localhost:${port}`)
    })
} catch (error:any) {
    console.log(`Error occurred: ${error.message}`)
}