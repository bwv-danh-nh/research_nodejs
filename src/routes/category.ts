import express from "express"
import CategoryValidator from '../validator/category';
import Middleware from '../middleware/handleValidation';
import IndexValidator from '../validator';
import CategoryController from '../controllers/category';

const router = express.Router();

    router.post('/category/create', CategoryValidator.checkCreateCategory(), Middleware.handleValidation, CategoryController.create)
    router.get('/category/list',CategoryController.list)
    router.get('/category/:id', IndexValidator.checkParamId(), Middleware.handleValidation, CategoryController.view)
    router.put('/category/:id/update', IndexValidator.checkParamId(), Middleware.handleValidation, CategoryController.update)
    router.delete('/category/:id/delete', IndexValidator.checkParamId(), Middleware.handleValidation, CategoryController.delete)

export default router;