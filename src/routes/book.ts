import express from "express"
import BookValidator from '../validator/book';
import Middleware from '../middleware/handleValidation';
import IndexValidator from '../validator';
import BookController from '../controllers/book';

const router = express.Router();

    router.post('/book/create', BookValidator.checkCreateBook(), Middleware.handleValidation, BookController.create)
    router.get('/book/list',BookController.list)
    router.get('/book/:id', IndexValidator.checkParamId(), Middleware.handleValidation, BookController.view)
    router.put('/book/:id/update', IndexValidator.checkParamId(), Middleware.handleValidation, BookController.update)
    router.delete('/book/:id/delete', IndexValidator.checkParamId(), Middleware.handleValidation, BookController.delete)

export default router;