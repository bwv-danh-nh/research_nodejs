import { body } from "express-validator";

class BookValidator{
    checkCreateBook(){
        return [
                body("title").notEmpty().withMessage("The title is required"),
                body("categoryId").notEmpty().withMessage("The category is required"),
                body("price").notEmpty().withMessage("The price is required").isNumeric().withMessage("The price must be num"),
                ]
    }
}
export default new BookValidator();