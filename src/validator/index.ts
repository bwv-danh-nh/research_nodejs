import { body, param } from "express-validator";

class IndexValidator{
    checkParamId (){
        return [
                param('id').notEmpty().withMessage('The param id should be not empty').isNumeric().withMessage('Param id must be num')
        ]
    }
}
export default new IndexValidator()