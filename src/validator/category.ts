import { body } from "express-validator";

class CategoryValidator{
    checkCreateCategory(){
        return [
                body("name").notEmpty().withMessage("The name is required"),
                body("description").isLength({max: 500}).withMessage('The description must be less than 500 character')
                ]
    }
}
export default new CategoryValidator();