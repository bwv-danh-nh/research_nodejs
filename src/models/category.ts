import { DatabaseError, DataTypes, Model, Sequelize } from "sequelize";
import sequelizeConnection from "../config/database";
import Book from './book';

interface CategoryAttributes {
 id: number,
 name: string,
 description: string,
 createdAt?: Date;
 updatedAt?: Date;
 deletedAt?: Date;
}

class Category extends Model<CategoryAttributes>{}
    Category.init({
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: {
            type: DataTypes.TEXT
        }

    },{
        timestamps: true,
        sequelize: sequelizeConnection,
        tableName: 'categories',
        paranoid: true
    });

Category.hasMany(Book, {as: 'books', foreignKey: 'categoryId'})
export default Category;