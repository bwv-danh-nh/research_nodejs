import { DatabaseError, DataTypes, Model } from "sequelize";
import sequelizeConnection from "../config/database";
import Category from './category';

interface BookAttributes {
 id: number,
 title: string,
 categoryId: string,
 price: number,
 image:string,
 description: string,
 createdAt?: Date;
 updatedAt?: Date;
 deletedAt?: Date;
}

class Book extends Model<BookAttributes>{}
    Book.init({
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
        categoryId: {
            type: DataTypes.INTEGER.UNSIGNED
        },
        price: {
            type: DataTypes.FLOAT
        },
        image: {
            type: DataTypes.STRING
        },
        description: {
            type: DataTypes.TEXT
        }

    },{
        timestamps: true,
        sequelize: sequelizeConnection,
        tableName: 'books',
        paranoid: true
    });

export default Book
