import Book from '../models/book';
import {NextFunction, Request, Response} from 'express';

class BookController {
    async create (req: Request, res:Response, next: NextFunction){
        try{
            const record = await Book.create({...req.body})
            return res.json({record, msg: "Successfully"})
        }   
        catch(err){
            return res.json({er: err, msg: "Failed"});
        }
    }

    async list (req: Request, res: Response, next: NextFunction) {
        try{
            const record = await Book.findAll();
            return res.json({record, msg: "Successfully"});
        }
        catch(err){
            return res.json({err, msg: "Failed"});
        }
    }

    async view (req:Request, res: Response, next: NextFunction) {
        try {
            const id = req.params.id;
            const record = await Book.findOne({ where: {id} });    
            return res.json({record, msg: "Successfully"})
        }
        catch (error) {
            return res.json({error, msg: "Failed"});
        }
    }

    async update (req:Request, res: Response) {
        try {
            const id = req.params.id;
            const record = await Book.findOne({ where: {id} });
            if(!record){
                return res.json({record, msg: `Can not find record has id ${id}`})
            }
            const update = await record.update({
                title: req.body.title,
                categoryId: req.body.categoryId,
                price: req.body.price,
                image: req.body.image,
                description: req.body.description
            })
            return res.json({record, msg: "Successfully"})
        }
        catch (error) {
            return res.json({error, msg: "Failed"});
        }
    }

    async delete (req:Request, res: Response) {
        try {
            const id = req.params.id;
            const record = await Book.findOne({ where: {id} });

            if(!record){
                return res.json({record, msg: `Can not find record has id ${id}`})
            }

            const deleteRecord = await record.destroy();
                return res.json({deleteRecord, msg: "Successfully"})
            }
        catch (error) {
            return res.json({error, msg: "Failed"});
        }
    }
}

export default new BookController()