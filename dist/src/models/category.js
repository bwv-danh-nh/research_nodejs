"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = __importDefault(require("../config/database"));
const book_1 = __importDefault(require("./book"));
class Category extends sequelize_1.Model {
}
Category.init({
    id: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: sequelize_1.DataTypes.TEXT
    }
}, {
    timestamps: true,
    sequelize: database_1.default,
    tableName: 'categories',
    paranoid: true
});
Category.hasMany(book_1.default, { as: 'books', foreignKey: 'categoryId' });
exports.default = Category;
//# sourceMappingURL=category.js.map