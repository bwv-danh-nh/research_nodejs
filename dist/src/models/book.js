"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = __importDefault(require("../config/database"));
class Book extends sequelize_1.Model {
}
Book.init({
    id: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    title: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false
    },
    categoryId: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED
    },
    price: {
        type: sequelize_1.DataTypes.FLOAT
    },
    image: {
        type: sequelize_1.DataTypes.STRING
    },
    description: {
        type: sequelize_1.DataTypes.TEXT
    }
}, {
    timestamps: true,
    sequelize: database_1.default,
    tableName: 'books',
    paranoid: true
});
exports.default = Book;
//# sourceMappingURL=book.js.map