"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_validator_1 = require("express-validator");
class CategoryValidator {
    checkCreateCategory() {
        return [
            (0, express_validator_1.body)("name").notEmpty().withMessage("The name is required"),
            (0, express_validator_1.body)("description").isLength({ max: 500 }).withMessage('The description must be less than 500 character')
        ];
    }
}
exports.default = new CategoryValidator();
//# sourceMappingURL=category.js.map