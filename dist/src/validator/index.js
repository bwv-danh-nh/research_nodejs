"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_validator_1 = require("express-validator");
class IndexValidator {
    checkParamId() {
        return [
            (0, express_validator_1.param)('id').notEmpty().withMessage('The param id should be not empty').isNumeric().withMessage('Param id must be num')
        ];
    }
}
exports.default = new IndexValidator();
//# sourceMappingURL=index.js.map