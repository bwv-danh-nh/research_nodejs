"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_validator_1 = require("express-validator");
class BookValidator {
    checkCreateBook() {
        return [
            (0, express_validator_1.body)("title").notEmpty().withMessage("The title is required"),
            (0, express_validator_1.body)("categoryId").notEmpty().withMessage("The category is required"),
            (0, express_validator_1.body)("price").notEmpty().withMessage("The price is required").isNumeric().withMessage("The price must be num"),
        ];
    }
}
exports.default = new BookValidator();
//# sourceMappingURL=book.js.map