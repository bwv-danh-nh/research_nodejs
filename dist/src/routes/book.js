"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const book_1 = __importDefault(require("../validator/book"));
const handleValidation_1 = __importDefault(require("../middleware/handleValidation"));
const validator_1 = __importDefault(require("../validator"));
const book_2 = __importDefault(require("../controllers/book"));
const router = express_1.default.Router();
router.post('/book/create', book_1.default.checkCreateBook(), handleValidation_1.default.handleValidation, book_2.default.create);
router.get('/book/list', book_2.default.list);
router.get('/book/:id', validator_1.default.checkParamId(), handleValidation_1.default.handleValidation, book_2.default.view);
router.put('/book/:id/update', validator_1.default.checkParamId(), handleValidation_1.default.handleValidation, book_2.default.update);
router.delete('/book/:id/delete', validator_1.default.checkParamId(), handleValidation_1.default.handleValidation, book_2.default.delete);
exports.default = router;
//# sourceMappingURL=book.js.map