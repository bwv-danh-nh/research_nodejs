"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const category_1 = __importDefault(require("../validator/category"));
const handleValidation_1 = __importDefault(require("../middleware/handleValidation"));
const validator_1 = __importDefault(require("../validator"));
const category_2 = __importDefault(require("../controllers/category"));
const router = express_1.default.Router();
router.post('/category/create', category_1.default.checkCreateCategory(), handleValidation_1.default.handleValidation, category_2.default.create);
router.get('/category/list', category_2.default.list);
router.get('/category/:id', validator_1.default.checkParamId(), handleValidation_1.default.handleValidation, category_2.default.view);
router.put('/category/:id/update', validator_1.default.checkParamId(), handleValidation_1.default.handleValidation, category_2.default.update);
router.delete('/category/:id/delete', validator_1.default.checkParamId(), handleValidation_1.default.handleValidation, category_2.default.delete);
exports.default = router;
//# sourceMappingURL=category.js.map