"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const category_1 = __importDefault(require("../models/category"));
class CategoryController {
    create(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const record = yield category_1.default.create(Object.assign({}, req.body));
                return res.json({ record, msg: "Successfully" });
            }
            catch (err) {
                return res.json({ er: err, msg: "Failed" });
            }
        });
    }
    list(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const record = yield category_1.default.findAll();
                return res.json({ record, msg: "Successfully" });
            }
            catch (err) {
                return res.json({ err, msg: "Failed" });
            }
        });
    }
    view(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.params.id;
                const record = yield category_1.default.findOne({ where: { id } });
                return res.json({ record, msg: "Successfully" });
            }
            catch (error) {
                return res.json({ error, msg: "Failed" });
            }
        });
    }
    update(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.params.id;
                const record = yield category_1.default.findOne({ where: { id } });
                if (!record) {
                    return res.json({ record, msg: `Can not find record has id ${id}` });
                }
                const update = yield record.update({
                    name: req.body.name,
                    description: req.body.description
                });
                return res.json({ record, msg: "Successfully" });
            }
            catch (error) {
                return res.json({ error, msg: "Failed" });
            }
        });
    }
    delete(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.params.id;
                const record = yield category_1.default.findOne({ where: { id } });
                if (!record) {
                    return res.json({ record, msg: `Can not find record has id ${id}` });
                }
                const deleteRecord = yield record.destroy();
                return res.json({ deleteRecord, msg: "Successfully" });
            }
            catch (error) {
                return res.json({ error, msg: "Failed" });
            }
        });
    }
}
exports.default = new CategoryController();
//# sourceMappingURL=category.js.map